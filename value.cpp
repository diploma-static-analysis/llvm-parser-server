//
// Created by vladislavkuznetsov on 31/03/24.
//
#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-no-recursion"
#include <iostream>
#include <llvm/IR/Constants.h>
#include <llvm/IR/TypedPointerType.h>
#include "cmake-build-debug/protobuf/type.pb.h"

void get_type(proto::type::Type* proto_type, const llvm::Type &type);

void get_array_type(proto::type::Type_ArrayType* proto_array_type, const llvm::ArrayType &array_type) {
  proto_array_type->set_count(array_type.getNumElements());
  get_type(proto_array_type->mutable_subtype(), *array_type.getElementType());
}

void get_integer_type(proto::type::Type_IntegerType* integer_proto_type, const llvm::IntegerType &integer_type) {
  integer_proto_type->set_size(integer_type.getBitWidth());
}

void get_pointer_type(proto::type::Type_PointerType* proto_pointer_type, const llvm::PointerType &pointer_type) {
  for (int i = 0; i < pointer_type.getNumContainedTypes(); i++) {
    get_type(proto_pointer_type->add_subtype(), *pointer_type.getContainedType(i));
  }
}

void get_void_type(proto::type::Type_VoidType* proto_void_type) {
  // nothing
}

void get_type(proto::type::Type* proto_type, const llvm::Type &type) {
  if (const auto *array_type = llvm::dyn_cast<llvm::ArrayType>(&type)) {
    get_array_type(proto_type->mutable_arraytype(), *array_type);
  } else if (const auto *function_type = llvm::dyn_cast<llvm::FunctionType>(&type)) {
    std::cout << "Expected FunctionType" << std::endl;
  } else if (const auto *integer_type = llvm::dyn_cast<llvm::IntegerType>(&type)) {
    get_integer_type(proto_type->mutable_integertype(), *integer_type);
  } else if (const auto *pointer_type = llvm::dyn_cast<llvm::PointerType>(&type)) {
    get_pointer_type(proto_type->mutable_pointertype(), *pointer_type);
  } else if (const auto *struct_type = llvm::dyn_cast<llvm::StructType>(&type)) {
    std::cout << "Expected StructType" << std::endl;
  } else if (const auto *target_ext_type = llvm::dyn_cast<llvm::TargetExtType>(&type)) {
    std::cout << "Expected TargetExtType" << std::endl;
  } else if (const auto *typed_pointer_type = llvm::dyn_cast<llvm::TypedPointerType>(&type)) {
    std::cout << "Expected TypedPointerType" << std::endl;
  } else if (const auto *vector_type = llvm::dyn_cast<llvm::VectorType>(&type)) {
    std::cout << "Expected VectorType" << std::endl;
  } else if (type.isVoidTy()) {
    get_void_type(proto_type->mutable_voidtype());
  } else throw std::invalid_argument("Unexpected Type child");
}