#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-no-recursion"
#include <iostream>
#include <llvm/IR/Constants.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/DerivedUser.h>
#include <llvm/IR/Operator.h>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Statepoint.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/Module.h"
#include "llvm/AsmParser/Parser.h"
#include "llvm/Analysis/MemorySSA.h"
#include "cmake-build-debug/protobuf/llvm.pb.h"
#include "cmake-build-debug/protobuf/llvm.grpc.pb.h"
#include "cmake-build-debug/protobuf/value.pb.h"
#include "grpc++/grpc++.h"
#include "boost/program_options.hpp"
#include "value.cpp"

// class (\w+) : public \w+ \{
// if (const auto* change me = llvm::dyn_cast<llvm::$1>(&llvm_instruction))

void get_value(proto::value::Value* proto_value, const llvm::Value &value);
void get_function(proto::value::Function* proto_function, const llvm::Function &llvm_function, bool with_blocks);

void get_alloca_inst(proto::value::AllocaInst* proto_alloca_inst, const llvm::AllocaInst &llvm_alloca_inst) {
  get_value(proto_alloca_inst->mutable_to(), llvm_alloca_inst);
}

void get_load_inst(proto::value::LoadInst* proto_load_inst, const llvm::LoadInst &llvm_load_inst) {
  get_value(proto_load_inst->mutable_to(), llvm_load_inst);
  get_value(proto_load_inst->mutable_from(), *llvm_load_inst.getOperand(0));
}

void get_return_inst(proto::value::ReturnInst* proto_return_inst, const llvm::ReturnInst &llvm_return_inst) {
  get_value(proto_return_inst->mutable_return_(), *llvm_return_inst.getOperand(0));
}

void get_call_inst(proto::value::CallInst* proto_call_inst, const llvm::CallInst &llvm_call_inst) {
  if (const auto *intrinsic_inst = llvm::dyn_cast<llvm::IntrinsicInst>(&llvm_call_inst)) {
    std::cout << "Expected IntrinsicInst" << std::endl;
  } else {
    const llvm::Function* called_function = llvm_call_inst.getCalledFunction();
    get_value(proto_call_inst->mutable_result(), llvm_call_inst);
    const unsigned argument_count = llvm_call_inst.arg_size();
    get_function(proto_call_inst->mutable_function(), *called_function, false);
    for (unsigned i = 0; i < argument_count; i++) {
      get_value(proto_call_inst->add_arguments(), *llvm_call_inst.getArgOperand(i));
    }
  }
}

void get_call_base(proto::value::CallBase* proto_call_base, const llvm::CallBase &llvm_call_base) {
  if (const auto *call_br_inst = llvm::dyn_cast<llvm::CallBrInst>(&llvm_call_base)) {
    std::cout << "Expected CallBrInst" << std::endl;
  } else if (const auto *call_inst = llvm::dyn_cast<llvm::CallInst>(&llvm_call_base)) {
    get_call_inst(proto_call_base->mutable_callinst(), *call_inst);
  } else if (const auto *gcstate_point_inst = llvm::dyn_cast<llvm::GCStatepointInst>(&llvm_call_base)) {
    std::cout << "Expected GCStatePointInst" << std::endl;
  } else if (const auto *invoke_inst = llvm::dyn_cast<llvm::InvokeInst>(&llvm_call_base)) {
    std::cout << "Expected InvokeInst" << std::endl;
  } else throw std::invalid_argument("Unexpected CallBase child");
}

void get_store_inst(proto::value::StoreInst* proto_store_inst, const llvm::StoreInst &llvm_store_inst) {
  get_value(proto_store_inst->mutable_to(), *llvm_store_inst.getPointerOperand());
  get_value(proto_store_inst->mutable_from(), *llvm_store_inst.getOperand(0));
}

void get_unary_instruction(proto::value::UnaryInstruction* proto_unary_instruction, const llvm::UnaryInstruction &llvm_unary_instruction) {
  if (const auto* unary_operator = llvm::dyn_cast<llvm::UnaryOperator>(&llvm_unary_instruction)) {
    std::cout << "Unary Operator" << std::endl;
  } else if (const auto *cast_inst = llvm::dyn_cast<llvm::CastInst>(&llvm_unary_instruction)) {
    std::cout << "Cast_inst" << std::endl;
  } else if (const auto* alloca_inst = llvm::dyn_cast<llvm::AllocaInst>(&llvm_unary_instruction)) {
    get_alloca_inst(proto_unary_instruction->mutable_allocainst(), *alloca_inst);
  } else if (const auto* load_inst = llvm::dyn_cast<llvm::LoadInst>(&llvm_unary_instruction)) {
    get_load_inst(proto_unary_instruction->mutable_loadinst(), *load_inst);
  } else if (const auto* vaarg_inst = llvm::dyn_cast<llvm::VAArgInst>(&llvm_unary_instruction)) {
    std::cout << "VAArgInst" << std::endl;
  } else if (const auto* extract_value_inst = llvm::dyn_cast<llvm::ExtractValueInst>(&llvm_unary_instruction)) {
    std::cout << "ExtractValueInst" << std::endl;
  } else if (const auto* freeze_inst = llvm::dyn_cast<llvm::FreezeInst>(&llvm_unary_instruction)) {
    std::cout << "FreezeInst" << std::endl;
  } else {
    std::cout << "Unexpected Pain in getUnaryInstruction" << std::endl;
  }
}

void get_branch_inst(proto::value::Instruction_BranchInst *proto_branch_inst, const llvm::BranchInst &branch_inst) {
  if (branch_inst.isConditional()) {
    auto proto_conditional = proto_branch_inst->mutable_conditionalbranchinst();
    if (branch_inst.getNumSuccessors() != 2) {
      throw std::invalid_argument("NumSucessors failed");
    }
    get_value(proto_conditional->mutable_condition(), *branch_inst.getCondition());
    proto_conditional->set_basicblocktruebranch(branch_inst.getSuccessor(0)->getNameOrAsOperand());
    proto_conditional->set_basicblockfalsebranch(branch_inst.getSuccessor(1)->getNameOrAsOperand());
  } else {
    auto proto_unconditional = proto_branch_inst->mutable_unconditionalbranchinst();
    proto_unconditional->set_basicblockname(branch_inst.getSuccessor(0)->getNameOrAsOperand());
  }
}

void get_cmp_inst(proto::value::Instruction_CmpInst *proto_cmp_inst, const llvm::CmpInst &cmp_inst) {
  proto_cmp_inst->set_isintpredicate(cmp_inst.isIntPredicate());
  proto_cmp_inst->set_isfloatpredicate(cmp_inst.isFPPredicate());
  get_value(proto_cmp_inst->mutable_left(), *cmp_inst.getOperand(0));
  get_value(proto_cmp_inst->mutable_result(), cmp_inst);
  switch (cmp_inst.getPredicate()) {
    case 32: proto_cmp_inst->mutable_type()->mutable_equality(); break;
    case 33: proto_cmp_inst->mutable_type()->mutable_notequality(); break;
    case 34: proto_cmp_inst->mutable_type()->mutable_greaterthan(); break;
    case 35: proto_cmp_inst->mutable_type()->mutable_greaterthanorequal(); break;
    case 36: proto_cmp_inst->mutable_type()->mutable_lessthan(); break;
    case 37: proto_cmp_inst->mutable_type()->clear_lessthanorequal(); break;
    case 38: proto_cmp_inst->mutable_type()->mutable_greaterthan(); break;
    case 39: proto_cmp_inst->mutable_type()->mutable_greaterthanorequal(); break;
    case 40: proto_cmp_inst->mutable_type()->mutable_lessthan(); break;
    case 41: proto_cmp_inst->mutable_type()->clear_lessthanorequal(); break;
    default: throw std::invalid_argument("Can't found predicate for " + cmp_inst.getPredicate());
  }
  get_value(proto_cmp_inst->mutable_right(), *cmp_inst.getOperand(1));
}

void get_instruction(proto::value::Instruction* proto_instruction, const llvm::Instruction &llvm_instruction) {
  if (llvm_instruction.getType() != nullptr) {
    get_type(proto_instruction->mutable_type(), *llvm_instruction.getType());
  }
  if (const auto *atomic_cmp_xchginst = llvm::dyn_cast<llvm::AtomicCmpXchgInst>(&llvm_instruction)) {
    std::cout << "Expected AtomicCmpXchginst" << std::endl;
  } else if (const auto *atomic_rmwinst = llvm::dyn_cast<llvm::AtomicRMWInst>(&llvm_instruction)) {
    std::cout << "Expected AtomicRMWInst" << std::endl;
  } else if (const auto *binary_operator = llvm::dyn_cast<llvm::BinaryOperator>(&llvm_instruction)) {
    auto* proto_binary_operator = proto_instruction->mutable_binaryoperator();
    if (binary_operator->getOpcode() == llvm::Instruction::Add) {
      get_value(proto_binary_operator->mutable_l(), *binary_operator->getOperand(0));
      get_value(proto_binary_operator->mutable_r(), *binary_operator->getOperand(1));
    }
  } else if (const auto *branch_inst = llvm::dyn_cast<llvm::BranchInst>(&llvm_instruction)) {
    get_branch_inst(proto_instruction->mutable_branchinst(), *branch_inst);
  } else if (const auto *call_base = llvm::dyn_cast<llvm::CallBase>(&llvm_instruction)) {
    get_call_base(proto_instruction->mutable_callbase(), *call_base);
  } else if (const auto *catch_returninst = llvm::dyn_cast<llvm::CatchReturnInst>(&llvm_instruction)) {
    std::cout << "Expected CatchReturninst" << std::endl;
  } else if (const auto *catch_switchinst = llvm::dyn_cast<llvm::CatchSwitchInst>(&llvm_instruction)) {
    std::cout << "Expected CatchSwitchinst" << std::endl;
  } else if (const auto *cleanup_returninst = llvm::dyn_cast<llvm::CleanupReturnInst>(&llvm_instruction)) {
    std::cout << "Expected CleanupReturninst" << std::endl;
  } else if (const auto *cmp_inst = llvm::dyn_cast<llvm::CmpInst>(&llvm_instruction)) {
    get_cmp_inst(proto_instruction->mutable_cmpinst(), *cmp_inst);
  } else if (const auto *extract_elementinst = llvm::dyn_cast<llvm::ExtractElementInst>(&llvm_instruction)) {
    std::cout << "Expected ExtractElementinst" << std::endl;
  } else if (const auto *fenceinst = llvm::dyn_cast<llvm::FenceInst>(&llvm_instruction)) {
    std::cout << "Expected Fenceinst" << std::endl;
  } else if (const auto *funclet_padinst = llvm::dyn_cast<llvm::FuncletPadInst>(&llvm_instruction)) {
    std::cout << "Expected FuncletPadinst" << std::endl;
  } else if (const auto *get_element_ptrinst = llvm::dyn_cast<llvm::GetElementPtrInst>(&llvm_instruction)) {
    std::cout << "Expected GetElementPtrinst" << std::endl;
  } else if (const auto *indirect_brinst = llvm::dyn_cast<llvm::IndirectBrInst>(&llvm_instruction)) {
    std::cout << "Expected IndirectBrinst" << std::endl;
  } else if (const auto *insert_elementinst = llvm::dyn_cast<llvm::InsertElementInst>(&llvm_instruction)) {
    std::cout << "Expected InsertElementinst" << std::endl;
  } else if (const auto *insert_valueinst = llvm::dyn_cast<llvm::InsertValueInst>(&llvm_instruction)) {
    std::cout << "Expected InsertValueinst" << std::endl;
  } else if (const auto *landing_padinst = llvm::dyn_cast<llvm::LandingPadInst>(&llvm_instruction)) {
    std::cout << "Expected LandingPadinst" << std::endl;
  } else if (const auto *phinode = llvm::dyn_cast<llvm::PHINode>(&llvm_instruction)) {
    std::cout << "Expected PHINode" << std::endl;
  } else if (const auto *resume_inst = llvm::dyn_cast<llvm::ResumeInst>(&llvm_instruction)) {
    std::cout << "Expected Resumeinst" << std::endl;
  } else if (const auto *return_inst = llvm::dyn_cast<llvm::ReturnInst>(&llvm_instruction)) {
    get_return_inst(proto_instruction->mutable_returninst(), *return_inst);
  } else if (const auto *select_inst = llvm::dyn_cast<llvm::SelectInst>(&llvm_instruction)) {
    std::cout << "Expected Selectinst" << std::endl;
  } else if (const auto *shuffle_vector_inst = llvm::dyn_cast<llvm::ShuffleVectorInst>(&llvm_instruction)) {
    std::cout << "Expected ShuffleVectorinst" << std::endl;
  } else if (const auto *store_inst = llvm::dyn_cast<llvm::StoreInst>(&llvm_instruction)) {
    get_store_inst(proto_instruction->mutable_storeinst(), *store_inst);
  } else if (const auto *switchinst = llvm::dyn_cast<llvm::SwitchInst>(&llvm_instruction)) {
    std::cout << "Expected Switchinst" << std::endl;
  } else if (const auto *unary_instruction = llvm::dyn_cast<llvm::UnaryInstruction>(&llvm_instruction)) {
    get_unary_instruction(proto_instruction->mutable_unaryinstruction(), *unary_instruction);
  } else if (const auto *unreachableinst = llvm::dyn_cast<llvm::UnreachableInst>(&llvm_instruction)) {
    std::cout << "Expected Unreachableinst" << std::endl;
  } else {
    std::cout << "Unexpected Pain in instruction" << std::endl;
  }
}

void get_block(proto::value::BasicBlock *basic_block, const llvm::BasicBlock &llvm_basic_block) {
  basic_block->set_name(llvm_basic_block.getNameOrAsOperand());
  for (const auto &instuction: llvm_basic_block) {
    get_instruction(basic_block->add_instructions(), instuction);
  }
  get_instruction(basic_block->mutable_terminator(), *llvm_basic_block.getTerminator());
}

void get_argument(proto::value::Argument* proto_argument, const llvm::Argument &llvm_argument) {
  proto_argument->set_name(llvm_argument.getNameOrAsOperand());
  get_type(proto_argument->mutable_type(), *llvm_argument.getType());
}

void get_function(proto::value::Function *proto_function, const llvm::Function &llvm_function, bool with_blocks) {
  proto_function->set_name(llvm_function.getNameOrAsOperand());
  if (with_blocks) {
    for (const auto &block : llvm_function) {
      get_block(proto_function->add_basicblocks(), block);
    }
  }
  for (llvm::Function::const_arg_iterator it = llvm_function.arg_begin(), end = llvm_function.arg_end(); it != end; ++it) {
    get_argument(proto_function->add_parameter(), *it);
  }
  get_type(proto_function->mutable_returntype(), *llvm_function.getReturnType());
}

void get_constant_int(proto::value::ConstantInt *proto_constant_int, const llvm::ConstantInt &llvm_constant_int) {
  proto_constant_int->set_value(llvm_constant_int.getSExtValue());
  proto_constant_int->set_width(llvm_constant_int.getBitWidth());
}

void get_constant_pointer_null(proto::value::ConstantPointerNull *proto_constant_null, const llvm::ConstantPointerNull &llvm_constant_null) {
  // nothing to do
}

void get_constant_data(proto::value::ConstantData *proto_constant_data, const llvm::ConstantData &llvm_constant_data) {
  if (const auto *constant_aggregate_zero = llvm::dyn_cast<llvm::ConstantAggregateZero>(&llvm_constant_data)) {
    std::cout << "Expected ConstantAggregateZero" << std::endl;
  } else if (const auto *constant_data_sequential = llvm::dyn_cast<llvm::ConstantDataSequential>(&llvm_constant_data)) {
    std::cout << "Expected ConstantDataSequential" << std::endl;
  } else if (const auto *constant_fp = llvm::dyn_cast<llvm::ConstantFP>(&llvm_constant_data)) {
    std::cout << "Expected ConstantFP" << std::endl;
  } else if (const auto *constant_int = llvm::dyn_cast<llvm::ConstantInt>(&llvm_constant_data)) {
    get_constant_int(proto_constant_data->mutable_constantint(), *constant_int);
  } else if (const auto *constant_pointer_null = llvm::dyn_cast<llvm::ConstantPointerNull>(&llvm_constant_data)) {
    get_constant_pointer_null(proto_constant_data->mutable_constantpointernull(), *constant_pointer_null);
  } else if (const auto *constant_target_none = llvm::dyn_cast<llvm::ConstantTargetNone>(&llvm_constant_data)) {
    std::cout << "Expected ConstantTargetNone" << std::endl;
  } else if (const auto *constant_token_none = llvm::dyn_cast<llvm::ConstantTokenNone>(&llvm_constant_data)) {
    std::cout << "Expected ConstantTokenNone" << std::endl;
  } else if (const auto *undef_value = llvm::dyn_cast<llvm::UndefValue>(&llvm_constant_data)) {
    std::cout << "Expected UndefValue" << std::endl;
  } else {
    std::cout << "CAN'T GET CONSTANT DATA" << std::endl;
  }
}

void get_global_variable(proto::value::GlobalVariable* global_variable, const llvm::GlobalVariable &llvm_global_variable) {
  global_variable->set_name(llvm_global_variable.getNameOrAsOperand());
}

void get_global_object(proto::value::GlobalObject* global_object, const llvm::GlobalObject &llvm_global_object) {
  if (const auto *function = llvm::dyn_cast<llvm::Function>(&llvm_global_object)) {
    std::cout << "Expected Function" << std::endl;
  } else if (const auto *global_ifunc = llvm::dyn_cast<llvm::GlobalIFunc>(&llvm_global_object)) {
    std::cout << "Expected GlobalIFunc" << std::endl;
  } else if (const auto *global_variable = llvm::dyn_cast<llvm::GlobalVariable>(&llvm_global_object)) {
    get_global_variable(global_object->mutable_globalvariable(), *global_variable);
  } else {
    throw std::invalid_argument("Failed to find subclass of GlobalObject");
  }
}

void get_global_value(proto::value::GlobalValue* global_value, const llvm::GlobalValue &llvm_global_value) {
  if (const auto *global_alias = llvm::dyn_cast<llvm::GlobalAlias>(&llvm_global_value)) {
    std::cout << "Expected GlobalAlias" << std::endl;
  } else if (const auto *global_object = llvm::dyn_cast<llvm::GlobalObject>(&llvm_global_value)) {
    get_global_object(global_value->mutable_globalobject(), *global_object);
  } else {
    throw std::invalid_argument("Failed to find subclass of GlobalValue");
  }
}

void get_constant(proto::value::Constant* proto_constant, const llvm::Constant &llvm_constant) {
  if (const auto *block_address = llvm::dyn_cast<llvm::BlockAddress>(&llvm_constant)) {
    std::cout << "Expected BlockAddress" << std::endl;
  } else if (const auto *constant_aggregate = llvm::dyn_cast<llvm::ConstantAggregate>(&llvm_constant)) {
    std::cout << "Expected ConstantAggregate" << std::endl;
  } else if (const auto *constant_data = llvm::dyn_cast<llvm::ConstantData>(&llvm_constant)) {
    get_constant_data(proto_constant->mutable_constantdata(), *constant_data);
  } else if (const auto *constant_expr = llvm::dyn_cast<llvm::ConstantExpr>(&llvm_constant)) {
    std::cout << "Expected ConstantExpr" << std::endl;
  } else if (const auto *dsolocal_equivalent = llvm::dyn_cast<llvm::DSOLocalEquivalent>(&llvm_constant)) {
    std::cout << "Expected DSOLocalEquivalent" << std::endl;
  } else if (const auto *global_value = llvm::dyn_cast<llvm::GlobalValue>(&llvm_constant)) {
    get_global_value(proto_constant->mutable_globalvalue(), *global_value);
  } else if (const auto *no_cfivalue = llvm::dyn_cast<llvm::NoCFIValue>(&llvm_constant)) {
    std::cout << "Expected NoCFIValue" << std::endl;
  } else {
    std::cout << "NO CONSTANT" << std::endl;
  }
}

void get_derived_user(proto::value::DerivedUser* proto_derived_user, const llvm::DerivedUser &llvm_derived_user) {
  if (const auto *memory_access = llvm::dyn_cast<llvm::MemoryAccess>(&llvm_derived_user)) {
    std::cout << "Expected MemoryAccess" << std::endl;
  } else {
    proto_derived_user->set_name(llvm_derived_user.getNameOrAsOperand());
  }
}

void get_user(proto::value::User* proto_user, const llvm::User &llvm_user) {
  if (const auto *constant = llvm::dyn_cast<llvm::Constant>(&llvm_user)) {
    get_constant(proto_user->mutable_constant(), *constant);
  } else if (const auto *derived_user = llvm::dyn_cast<llvm::DerivedUser>(&llvm_user)) {
    get_derived_user(proto_user->mutable_deriveduser(), *derived_user);
  } else if (const auto *instruction = llvm::dyn_cast<llvm::Instruction>(&llvm_user)) {
    get_instruction(proto_user->mutable_instruction(), *instruction);
  } else if (const auto *op = llvm::dyn_cast<llvm::Operator>(&llvm_user)) {
    std::cout << "Expected Operator" << std::endl;
  } else {
    std::cout << "CAN'T GET USER\n";
  }
}

void get_value(proto::value::Value* proto_value, const llvm::Value &value) {
  get_type(proto_value->mutable_type(), *value.getType());
  if (value.hasName()) {
    // It have name so we can ignore other things
    proto_value->set_name(value.getName().str());
  } else if (const auto *argument = llvm::dyn_cast<llvm::Argument>(&value)) {
    get_argument(proto_value->mutable_argument(), *argument);
  } else if (const auto *basic_block = llvm::dyn_cast<llvm::BasicBlock>(&value)) {
    std::cout << "Expected BasicBlock" << std::endl;
  } else if (const auto *inline_asm = llvm::dyn_cast<llvm::InlineAsm>(&value)) {
    std::cout << "Expected InlineAsm" << std::endl;
  } else if (const auto *metadata_as_value = llvm::dyn_cast<llvm::MetadataAsValue>(&value)) {
    std::cout << "Expected MetadataAsValue" << std::endl;
  } else if (const auto *user = llvm::dyn_cast<llvm::User>(&value)) {
    get_user(proto_value->mutable_user(), *user);
  } else {
    std::cout << "CAN'T GET VALUE\n";
  }
}

void get_module(proto::value::Module* module, llvm::Module &llvm_module) {
/*  std::cout << "Load module" << std::endl;*/
  const llvm::SymbolTableList<llvm::Function> &list = llvm_module.getFunctionList();
  for (const auto &function: list) {
    get_function(module->add_functions(), function, true);
  }
}

class LLVMReadService final : public proto::LLVMParseService::Service {
public:
  grpc::Status Parse(::grpc::ServerContext *context, const ::proto::LLVMRequest *request, ::proto::LLVMResponse *response) override {
    std::cout << "Server: get text: " << request->file() << std::endl;
    llvm::SMDiagnostic Err;
    llvm::LLVMContext Context;
    const std::unique_ptr<llvm::Module> Mod = llvm::parseAssemblyString(request->file(), Err, Context);
    if (!Mod) {
      Err.print("Server", llvm::errs());
      return grpc::Status::CANCELLED;
    }
    proto::value::Module* module = response->mutable_module();
    get_module(module, *Mod);
    return grpc::Status::OK;
  }
};

class options;

void protobuf_version() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  grpc::ServerBuilder builder;
  builder.AddListeningPort("0.0.0.0:50051", grpc::InsecureServerCredentials());

  LLVMReadService my_service;
  builder.RegisterService(&my_service);

  std::unique_ptr server(builder.BuildAndStart());
  server->Wait();
  google::protobuf::ShutdownProtobufLibrary();
}

void debug(const boost::program_options::variables_map& vm) {
  std::string file;
  if (vm.count("file")) {
    file = vm["file"].as<std::string>();
  } else {
    std::cerr << "Error: Mode is not specified." << std::endl;
    return;
  }

  llvm::SMDiagnostic Err;
  llvm::LLVMContext Context;
  const std::unique_ptr<llvm::Module> Mod = parseIRFile(file, Err, Context);
  if (!Mod) {
    std::cerr << "Error while parse file";
    return;
  }
  proto::value::Module module;
  get_module(&module, *Mod);
  std::cout << module.Utf8DebugString() << std::endl;

  std::string json_string;
  google::protobuf::util::MessageToJsonString(module, &json_string);
  std::cout << json_string << std::endl;

  // std::cout << llvm::outs();
}

int main(int argc, char **argv) {
  try {
    boost::program_options::options_description description("Allowed options");
    description.add_options()
        ("help", "produce help message")
        ("mode", boost::program_options::value<std::string>(), "set mode: server or plain")
        ("file", boost::program_options::value<std::string>(), "filename for plain mode");

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, description), vm);
    boost::program_options::notify(vm);

    if (vm.count("help")) {
      std::cout << description << std::endl;
      return 0;
    }

    std::string mode;
    if (vm.count("mode")) {
      mode = vm["mode"].as<std::string>();
    } else {
      std::cerr << "Error: Mode is not specified." << std::endl;
      return 1;
    }

    if (mode == "server") {
      protobuf_version();
    }
    if (mode == "plain") {
      debug(vm);
    }
    if (mode != "server" && mode != "plain") {
      std::cerr << "Error: Wrong mode: " << mode << std::endl;
      return 1;
    }
  } catch (const std::exception& e) {
    std::cerr << "Error: " << e.what() << std::endl;
    return 1;
  }
  return 0;
}

#pragma clang diagnostic pop